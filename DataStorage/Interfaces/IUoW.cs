﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStorage.Interfaces
{
    public interface IUoW : IDisposable
    {
        IRepository<People> Peoples { get; }
        IRepository<PeopleSalary> Salaries { get; }
        void Save();
    }
}
