﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataStorage.Interfaces;

namespace DataStorage.Repos
{
    class PeopleRepository : IRepository<People>
    {
        private PeoplesModelContainer _db;

        public PeopleRepository(PeoplesModelContainer context)
        {
            _db = context;
        }

        public IEnumerable<People> GetAll()
        {
            return _db.PeopleSet;
        }

        public People Get(int id)
        {
            return _db.PeopleSet.Find(id);
        }

        public IEnumerable<People> Find(Func<People, bool> predicate)
        {
            return _db.PeopleSet.Where(predicate).ToList();
        }

        public void Create(People item)
        {
            _db.PeopleSet.Add(item);
        }

        public void Update(People item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var people = _db.PeopleSet.Find(id);
            if (people != null)
            {
                _db.PeopleSet.Remove(people);
            }
        }
    }
}
