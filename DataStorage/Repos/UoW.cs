﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataStorage.Interfaces;

namespace DataStorage.Repos
{
    public class UoW : IUoW
    {
        private PeoplesModelContainer _db;
        private PeopleRepository peopleRepo;
        private PeopleSalaryReposirory peopleSalaryRepo;
        private bool _disposed;

        public UoW(string connStr)
        {
            _db = new PeoplesModelContainer(connStr);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IRepository<People> Peoples => 
            peopleRepo ?? (peopleRepo = new PeopleRepository(_db));

        public IRepository<PeopleSalary> Salaries =>
            peopleSalaryRepo ?? (peopleSalaryRepo = new PeopleSalaryReposirory(_db));

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
