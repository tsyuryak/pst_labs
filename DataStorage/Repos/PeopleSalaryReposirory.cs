﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataStorage.Interfaces;

namespace DataStorage.Repos
{
    class PeopleSalaryReposirory : IRepository<PeopleSalary>
    {
        private PeoplesModelContainer _db;

        public PeopleSalaryReposirory(PeoplesModelContainer context)
        {
            _db = context;
        }

        public IEnumerable<PeopleSalary> GetAll()
        {
            return _db.PeopleSalarySet;
        }

        public PeopleSalary Get(int id)
        {
            return _db.PeopleSalarySet.Find(id);
        }

        public IEnumerable<PeopleSalary> Find(Func<PeopleSalary, bool> predicate)
        {
            return _db.PeopleSalarySet.Where(predicate).ToList();
        }

        public void Create(PeopleSalary item)
        {
            _db.PeopleSalarySet.Add(item);
        }

        public void Update(PeopleSalary item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var ps = _db.PeopleSalarySet.Find(id);
            if (ps != null)
                _db.PeopleSalarySet.Remove(ps);
        }
    }
}
