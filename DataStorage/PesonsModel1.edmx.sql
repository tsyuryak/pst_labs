
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/13/2016 19:56:31
-- Generated from EDMX file: C:\Users\Bell\Documents\Visual Studio 2015\Projects\PSTLabsEx\DataStorage\PesonsModel1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BaseSalaries];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_PeoplePeopleSalary]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PeopleSalarySet] DROP CONSTRAINT [FK_PeoplePeopleSalary];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[PeopleSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PeopleSet];
GO
IF OBJECT_ID(N'[dbo].[PeopleSalarySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PeopleSalarySet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'PeopleSet'
CREATE TABLE [dbo].[PeopleSet] (
    [PeopleID] int IDENTITY(1,1) NOT NULL,
    [FIO] nchar(200)  NULL
);
GO

-- Creating table 'PeopleSalarySet'
CREATE TABLE [dbo].[PeopleSalarySet] (
    [SalaryDate] datetime  NOT NULL,
    [Salary] float  NOT NULL,
    [People_PeopleID] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [PeopleID] in table 'PeopleSet'
ALTER TABLE [dbo].[PeopleSet]
ADD CONSTRAINT [PK_PeopleSet]
    PRIMARY KEY CLUSTERED ([PeopleID] ASC);
GO

-- Creating primary key on [SalaryDate] in table 'PeopleSalarySet'
ALTER TABLE [dbo].[PeopleSalarySet]
ADD CONSTRAINT [PK_PeopleSalarySet]
    PRIMARY KEY CLUSTERED ([SalaryDate] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [People_PeopleID] in table 'PeopleSalarySet'
ALTER TABLE [dbo].[PeopleSalarySet]
ADD CONSTRAINT [FK_PeoplePeopleSalary]
    FOREIGN KEY ([People_PeopleID])
    REFERENCES [dbo].[PeopleSet]
        ([PeopleID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PeoplePeopleSalary'
CREATE INDEX [IX_FK_PeoplePeopleSalary]
ON [dbo].[PeopleSalarySet]
    ([People_PeopleID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------