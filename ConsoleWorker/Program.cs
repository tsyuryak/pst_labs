﻿using System;
using System.Linq;
using System.Threading.Tasks;
using XMLHandler;
using System.Configuration;
using System.IO;
using System.Threading;
using Sender;

namespace ConsoleWorker
{
    /// <summary>
    /// Клиент
    /// </summary>
    class Program
    {
 
        private static CancellationTokenSource _cts = new CancellationTokenSource();

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;

            //Служебные папки, устанавливаются в App.config
            //Если файл валидный перемещается сюда
            var validFiles = ConfigurationManager.AppSettings["valid_files"];
            //Если файл с ошибками перемещается сюда
            var invalidFiles = ConfigurationManager.AppSettings["invalid_files"];
            //После отправки файлы перемещаются сюда
            var sentFiles = ConfigurationManager.AppSettings["sent_files"];
            //Файл схемы
            var schemaFile = ConfigurationManager.AppSettings["schema_file"];
            //Общаяя папка в которую помещаются файлы для отправки
            var xmlFolder = ConfigurationManager.AppSettings["xml_folder"];

            try
            {
                CheckFolders(
                    validFiles,
                    invalidFiles,
                    sentFiles,
                    Path.GetDirectoryName(schemaFile),
                    xmlFolder);
            }
            catch (Exception e)
            {
                ShowErrorMessage(e.Message);
                return;
            }
            

            var xmlValidator = new XMLValidator
            {
                XMLSchemaPath = schemaFile,
                DocFolder = xmlFolder
            };

            xmlValidator.Error += (o, e) =>
            {
                //Если файл не валидный переносим в
                //соответстующую папку.
                ShowErrorMessage(e.Message);
                MoveFile(e.FilePath, invalidFiles);
                Console.WriteLine(new string('=', 80));
            };

            xmlValidator.FileValid += (o, e) =>
            {
                //Переносит в паку валидных файлов
                Console.WriteLine(e.Message);
                MoveFile(e.FilePath, validFiles);
                Console.WriteLine(new string('=', 80));
            };

            var extractor = new XMLExtractor
            {
                ValidFilesFolder = validFiles
            };

            var sender = new PersonSender();

            extractor.ReadyToSend += (o, e) =>
            {
                Task.Factory.StartNew(() =>
                {

                    try
                    {
                        //Отправляет данные из валидного файла.
                        sender.Send(e.FIO, e.SalaryDate, e.Salary, _cts.Token);
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc);
                        _cts.Cancel();
                    }
                    
                    
                }).Wait();
            };

            extractor.ReadyToMove += (o, e) =>
            {
                //Переносит файлы в папку отправленных.
                MoveFile(e.File, sentFiles);
            };
        
            sender.Sent += (o, e) =>
            {
                Console.Write("+");
            };

            Task.Factory.StartNew(() =>
            {
                try
                {
                    //Проверка на валидность.
                    xmlValidator.Validate();
                }
                catch (ArgumentNullException e)
                {
                    ShowErrorMessage(e.Message);
                }
                catch (ArgumentException e)
                {
                    ShowErrorMessage(e.Message);
                }
                
            }).Wait();

            Task.Factory.StartNew(() =>
            {
                //Разбор xml
                extractor.Parse(_cts.Token);
            }).Wait();
        }

        /// <summary>
        /// Переностит файл в указанную папку, унифицирует имя файла.
        /// </summary>
        private static void MoveFile(string filePath1, string filePath2)
        {
            var path1 = filePath1;
            var path2 = Path.Combine(filePath2, $"{Guid.NewGuid()}.xml");
            Console.WriteLine($"Файл {path1} перемещен в {path2}");
            File.Move(path1, path2);
        }

        /// <summary>
        /// Выделяет сообщения красным.
        /// </summary>
        public static void ShowErrorMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Green;
        }

        /// <summary>
        /// Проверяет наличие папок указанных в App.config,
        /// создает по указанному пути, если папки не существует.
        /// </summary>
        /// <param name="folders"></param>
        public static void CheckFolders(params string[] folders)
        {
            foreach (var folder in folders.Where(p=>!Directory.Exists(p)))
            {
                //Предполагается, что пути в App.config указаны.
                if (string.IsNullOrEmpty(folder))
                {
                    throw new Exception("Необоходимо указать служебные папки в App.config");
                }

                try
                {
                    Directory.CreateDirectory(folder);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
    }
}
