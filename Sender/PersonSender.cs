﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Contract;

namespace Sender
{
    public class PersonSender
    {
        public delegate void SenderHandler(object sender, SenderEventArgs e);

        public event SenderHandler Sent;

        private IContract service;

        public PersonSender()
        {
            Init();
        }

        private void Init()
        {
            var channelFactory =
                new ChannelFactory<IContract>(new BasicHttpBinding(), 
                new EndpointAddress("http://localhost:8088/PersonService"));

            service = channelFactory.CreateChannel();

        }

        public void Send(string fio, DateTime salaryDate, double salary, CancellationToken token)
        {
            try
            {
                if (token.IsCancellationRequested) return;
                service.AddPersonSalary(fio, salaryDate, salary);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            
            Sent?.Invoke(this, new SenderEventArgs($"Отправлены данные: {fio} {salaryDate} {salary}"));
        }

        
    }

    public class SenderEventArgs
    {
        public string Message { get; private set; }

        public SenderEventArgs(string message)
        {
            Message = message;
        }
    }
}
