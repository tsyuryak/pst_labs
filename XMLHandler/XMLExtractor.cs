﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Linq;

namespace XMLHandler
{
    /// <summary>
    /// Разбирает валидный файл. 
    /// </summary>
    public class XMLExtractor
    {

        public string ValidFilesFolder { get; set; }
        public string Extention { get; set; } = ".xml";

        public delegate void XMLExtractorHandler(object sender, XMLExtractorEventArgs e);

        public event XMLExtractorHandler ReadyToSend;
        public event XMLExtractorHandler ReadyToMove;

        public void Parse(CancellationToken token)
        {
            //Выбираем файлы с расширением *.xml
            var files = Directory.GetFiles(ValidFilesFolder).Where(p => Path.GetExtension(p) == Extention);

            foreach (var file in files)
            {
                if(token.IsCancellationRequested) return;;
                //Разбор XML
                var document = XDocument.Load(file);
                var desc = document.Descendants();
                var personsEl = desc.Elements("Person");
                
                var persons = personsEl.Select(p => 
                    new  {
                        FIO = p.Element("FIO").Value
                        , SalaryDate = p.Element("SalaryDate").Value
                        , Salary = p.Element("Salary").Value
                }).ToList();

                foreach (var person in persons)
                {
                    //Инициирум событие для отправки сообщения.
                    var sd = DateTime.Parse(person.SalaryDate);
                    var salary = double.Parse(person.Salary.Replace(".", ","));
                    ReadyToSend?.Invoke(this, new XMLExtractorEventArgs(person.FIO, sd, salary));
                }
                //Событие - данные разобранного файла обработаны.
                ReadyToMove?.Invoke(this, new XMLExtractorEventArgs(file));
            }
        }
    }

    public class XMLExtractorEventArgs
    {
        public string FIO { get; private set; }
        public DateTime SalaryDate { get; private set; }
        public double Salary { get; private set; }
        public string File { get; private set; }

        public XMLExtractorEventArgs(string file)
        {
            File = file;
        }

        public XMLExtractorEventArgs(string fio, DateTime salatyDate, double salary)
        {
            FIO = fio;
            SalaryDate = salatyDate;
            Salary = salary;
        }
    }
}
