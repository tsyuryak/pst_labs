﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Schema;

namespace XMLHandler
{
    /// <summary>
    /// Проверяет файл на валидность согласно шаблону.
    /// </summary>
    public class XMLValidator
    {
        /// <summary>
        /// Путь до шаблона.
        /// </summary>
        public string XMLSchemaPath { get; set; }
        public string DocFolder { get; set; }
        public string Extention { get; set; } = ".xml";

        public delegate void XMLValidatorHandler(object sender, XMLValidatorEventArgs e);

        public event XMLValidatorHandler Error;
        public event XMLValidatorHandler FileValid;


        public void Validate()
        {
            CheckFolder(DocFolder);
            CheckSchema(XMLSchemaPath);
            
            var files = Directory.GetFiles(DocFolder).Where(p => Path.GetExtension(p) == Extention);

            var schema = new XmlSchemaSet();
            schema.Add("", XMLSchemaPath);
            
            foreach (var file in files)
            {
                var error = false;
                XDocument doc;

                try
                {
                    doc = XDocument.Load(file);
                    
                }
                catch (Exception e)
                {
                    Error?.Invoke(this, new XMLValidatorEventArgs(($"Ошибка в файле: {Path.GetFileName(file)} {e.Message}"), Path.GetFullPath(file)));
                    continue;
                }

                doc.Validate(schema, (o, e) =>
                {
                    Error?.Invoke(this,
                        new XMLValidatorEventArgs(($"Ошибка в файле: {Path.GetFileName(file)} {e.Message}"), Path.GetFullPath(file)));
                    error = true;
                });

                if (!error)
                    FileValid?.Invoke(this, new XMLValidatorEventArgs(($"Успешно обработан: {Path.GetFileName(file)} "), Path.GetFullPath(file)));            
                    
            }
        }

        private static void CheckSchema(string schema)
        {
            if (string.IsNullOrEmpty(schema))
            {
                throw new ArgumentNullException("Путь к схеме не указан.");
            }

            if (!File.Exists(schema))
                throw new ArgumentException($"Файла {schema} не существует");
        }

        private static void CheckFolder(string docFolder)
        {
            if (string.IsNullOrEmpty(docFolder))
            {
                throw new ArgumentNullException("Путь к директории не указан.");
            }
        }
    }

    public class XMLValidatorEventArgs
    {
        public string Message { get; private set; }
        public string FilePath { get; private set; }

        public XMLValidatorEventArgs(string message, string filePath)
        {
            Message = message;
            FilePath = filePath;
        }
    }
}
