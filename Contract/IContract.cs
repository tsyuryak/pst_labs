﻿using System;
using System.ServiceModel;

namespace Contract
{
    [ServiceContract]
    public interface IContract
    {
        [OperationContract]
        void AddPersonSalary(string fio, DateTime salaryDate, double salary);
    }
}
