﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Service.Models;

namespace Service.Interfaces
{
    interface IService
    {
        void SavePeople(PeopleModel people);
        void Dispose();
    }
}
