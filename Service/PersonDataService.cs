﻿using System;
using System.Threading;
using Contract;
using DataStorage.Repos;
using Service.Models;

namespace Service
{
    class PersonDataService : IContract
    {
   
        public void AddPersonSalary(string fio, DateTime salaryDate, double salary)
        {
            //Делаем небольшие рандомные задержки для теста приема/записи данных
            var rnd = new Random();
            Thread.Sleep(rnd.Next(100, 8000));
            var pm = new PeopleModel
            {
                FIO = fio,
                SalaryDate = salaryDate,
                Salary = salary
            };

            var inData = $"{pm.FIO} {pm.Salary} {pm.SalaryDate}";

            try
            {
                var srv = new Service(new UoW("name=PesonsModel1Container"));
                srv.SavePeople(pm);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"{inData} добавлен");
                Console.ForegroundColor = ConsoleColor.Gray;
                srv.Dispose();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{inData} возможно добавлен ранее.");
                Console.WriteLine(e.Message);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            
            Thread.Sleep(rnd.Next(10, 500));
        }
    }
}
