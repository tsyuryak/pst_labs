﻿using System;
using System.ServiceModel;

namespace Service
{
    /// <summary>
    /// Сервис, добавляющий входящие данные.
    /// Запускается от имени администратора.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Приемник данных из XML";
           
            
            using (var serviceHost = new ServiceHost(typeof(PersonDataService), new Uri("http://localhost:8088/PersonService")))
            {
                serviceHost.Open();

                Console.WriteLine("Для завершения нажмите <Any Key>.");
                Console.ReadKey();
            }

        }
    }
}
