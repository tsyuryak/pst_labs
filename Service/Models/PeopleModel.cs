﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Models
{
    public class PeopleModel
    {
        public string FIO { get; set; }
        public DateTime SalaryDate { get; set; }
        public double Salary { get; set; }
    }
}
