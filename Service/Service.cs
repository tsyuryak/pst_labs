﻿using DataStorage;
using DataStorage.Interfaces;
using Service.Interfaces;
using Service.Models;

namespace Service
{
    class Service : IService
    {
        IUoW Database { get; }

        public Service(IUoW uow)
        {
            Database = uow;
        }

        public void SavePeople(PeopleModel pm)
        {
            var people = new People
            {
                FIO = pm.FIO
            };

            var ps = new PeopleSalary
            {
                People = people,
                Salary = pm.Salary,
                SalaryDate = pm.SalaryDate
            };

            Database.Peoples.Create(people);
            Database.Salaries.Create(ps);
            Database.Save();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
